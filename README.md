# vue-calendar
A Vue component that displays a simple calendar.

## Installation
First install the package from npm
```
npm install @drcake/vue-calendar
```
Then, register the component with Vue
```
import VueCalendar from '@drcake/vue-calendar';

Vue.component('vue-calendar', VueCalendar);
```

### moment js
Vue Calendar uses the default moment settings, to change these, first import moment then change properties as necessary

Example changing the locale to British:
```
import moment from 'moment';

moment.locale('en_GB');
```

### browser
Include the script file, then install the component
```
<script type="text/javascript" src="node_modules/vuejs/dist/vue.min.js"></script>
<script type="text/javascript" src="node_modules/vue-calendar/dist/vue-calendar.min.js"></script>
<script type="text/javascript">
  Vue.use(VueCalendar);
</script>
```

## Usage
Once installed, it can be used like any other vue component
```
<vue-calendar></vue-calendar>
```

## Customize
### Buttons
You can change the content of the 'next' and 'previous' buttons via slots.

Vue Calendar accepts two slots: `previous` and `next`. These input content into the previous and next buttons respectively.

Example:
```
<vue-calendar>

      <template slot="previous">
        <span>Previous</span>
      </template>
      
      <template slot="next">
          <span>Next</span>
      </template>

</vue-calendar>
```

## Dates
Vue Calendar also accepts an `Array` prop called `events`. Dates in the `YYY-MM-DD` format are accepted an if a date shown is listed in this array, a border is added to that date in the calendar.

Example:
```
<vue-calendar v-bind:events="['2018-01-01, '2018-05-01']" ></vue-calendar>
```
